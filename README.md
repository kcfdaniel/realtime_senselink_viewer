# realtime_senselink_viewer

## Project setup

``` shell
npm install
```

Make a copy of .env.example as .env in the project root directory:

**Linux/MacOS**:

```shell
cp .env.example .env
```

**Windows**:

```shell
copy .env.example .env
```

Open .env in a text editor, set the following environment variables as needed:

### SenseLink <!-- omit in toc -->

- `SENSELINK_HOST`: host address ond port of SenseLink
- `APP_KEY`: app key of SenseLink
- `APP_SECRET`: app secret of SenseLink

### Compiles and hot-reloads for development

``` shell
npm run serve
```

### Compiles and minifies for production

``` shell
npm run build
```

### Lints and fixes files

``` shell
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
