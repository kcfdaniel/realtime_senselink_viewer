import Vue from 'vue'
import Vuex from 'vuex'
import CryptoJS from 'crypto-js'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        test: "hello world",
        devices: [{
            name: "qwert",
            id: "12345",
            temperature: 36.3,
        }],
        current_device_id: "12345",
    },
    mutations: {
        load_devices: (state, payload) => {
            let devices = payload
            state.devices = devices
            state.current_device_id = devices[0].id
        },
        on_new_record(state, payload) {
            let record = payload
            console.log({ record });
            let device = state.devices.find(device => device.id === record.sn)

            if ('bodyTemperature' in record) {
                device.temperature = record.bodyTemperature
            }
        }
    },
    actions: {
        load_devices(context) {
            var app_key = process.env.VUE_APP_APP_KEY
            var app_secret = process.env.VUE_APP_APP_SECRET
            var host = process.env.VUE_APP_SENSELINK_HOST

            var timestamp = Math.round(+new Date())
            var sign = CryptoJS.MD5(timestamp + '#' + app_secret).toString()
            var canonial_uri = "/api/v1/device"

            axios.get(host + canonial_uri, {
                    params: {
                        app_key,
                        sign,
                        timestamp,
                    }
                })
                .then((response) => {
                    console.log(response)
                    let devices = response.data.data.data.map(device => {
                        device = device.device
                        return {
                            name: device.name,
                            id: device.sn,
                            temperature: 36
                        }
                    })
                    context.commit('load_devices', devices)
                }, (error) => {
                    console.log(error)
                });
        },
        SOCKET_on_new_record(context, payload) {
            let record = payload
            context.commit('on_new_record', record)
        }
    },
    modules: {}
})