module.exports = {
    "devServer": {
        "proxy": {
            "^/api": {
                "target": "https://link.bi.sensetime.com"
            }
        }
    },
    "transpileDependencies": [
        "vuetify"
    ]
}